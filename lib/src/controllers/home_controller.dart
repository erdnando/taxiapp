import 'package:get/state_manager.dart';
import 'package:taxiapp/src/api/users_api.dart';
import 'package:taxiapp/src/models/user.dart';
import 'package:taxiapp/src/pages/profile_page.dart';
import 'package:get/route_manager.dart';

class HomeController extends GetxController {
  int _counter = 0;
  List<User> _users = [];
  bool _loading = true;

  int get counter => _counter;
  List<User> get users => _users;
  bool get loading => _loading;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    //Carga usuarios al iniciar la vista
    this.loadUsers();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() {
    this._counter++;
    update(['text']);
  }

  Future<void> loadUsers() async {
    final data = await UsersApi.instance.getUsers(1);
    this._users = data;
    this._loading = false;
    update(['users']);
  }

  Future<void> showUserProfile(User user) async {
    final result = await Get.to<String>(ProfilePage(), arguments: user);

    if (result != null) {
      print('resutado: $result');
    }
  }
}
