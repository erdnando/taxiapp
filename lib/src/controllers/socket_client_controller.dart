import 'dart:async';
import 'package:faker/faker.dart';
//import 'package:get/get_rx/src/rx_types/rx_types.dart';
//import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class SocketClientController extends GetxController {
  RxString _message = "".obs;
  RxString get message => _message;

  RxInt _counter = 0.obs;
  RxString _search = "".obs;

  Timer _timer, _timerCounter;
  Faker _faker = Faker();

  @override
  void onInit() {
    super.onInit();
    _init();
  }

  _init() {
    /*ever(_counter, (_) {
      print("counter has been changed ${_counter.value}");
    });*/
    /*once(_counter, (_) {
      print("counter has been changed ${_counter.value}");
    });*/

    debounce(_search, (_) {
      print(_search.value);
    }, time: Duration(milliseconds: 500));

    _timer = Timer.periodic(Duration(seconds: 5), (timer) {
      _message.value = _faker.lorem.sentence();
    });

    _timerCounter = Timer.periodic(Duration(seconds: 1), (timer) {
      _counter.value++;
    });
  }

  @override
  void onClose() {
    if (_timer != null) {
      _timer.cancel();
    }

    if (_timerCounter != null) {
      _timerCounter.cancel();
    }
    super.onClose();
  }

  void setSearchText(String value) {
    _search.value = value;
  }
}
