import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:taxiapp/src/models/user.dart';

class ProfileController extends GetxController {
  User _user;
  User get user => _user;

  String _inputText = '';

  @override
  void onInit() {
    super.onInit();

    this._user = Get.arguments as User;
  }

  void onInputTextChange(String text) {
    this._inputText = text;
  }

  void goToBckWithData() {
    if (this._inputText.trim().length > 0) {
      Get.back(result: this._inputText); //return data from page
    } else {
      //error case
      if (Platform.isAndroid) {
        showCupertinoModalPopup(
            context: Get.overlayContext,
            builder: (_) => CupertinoActionSheet(
                  title: Text('ERROR'),
                  message: Text('Ingrese un dato valido'),
                  cancelButton: CupertinoActionSheetAction(
                    child: Text('Aceptar'),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                ));
      } else {
        Get.dialog(AlertDialog(
          title: Text('ERROR'),
          content: Text('Ingrese un valor valido'),
          actions: [
            FlatButton(
                onPressed: () {
                  Get.back();
                },
                child: Text('ACEPTAR'))
          ],
        ));
      }
    }
  }
}
