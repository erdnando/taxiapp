import 'package:get/state_manager.dart';
import 'package:get/route_manager.dart';
import 'package:taxiapp/src/pages/HomePage.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();

    Future.delayed(Duration(seconds: 2), () {
      Get.off(HomePage(), transition: Transition.zoom);
    });
  }

  @override
  void onClose() {
    super.onClose();
  }
}
