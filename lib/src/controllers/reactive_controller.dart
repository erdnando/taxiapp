//import 'package:get/get_state_manager/get_state_manager.dart';

import 'dart:async';

import 'package:get/get.dart';
import 'package:taxiapp/src/controllers/socket_client_controller.dart';
import 'package:taxiapp/src/models/pet.dart';

class ReactiveController extends GetxController {
  RxInt counter = 0.obs;
  RxString currentDate = ''.obs;

  RxList<String> items = <String>[].obs;
  RxMap<String, dynamic> mapItems = Map<String, dynamic>().obs;

  Pet myPet = Pet(name: 'kitty', age: 1);
  StreamSubscription<String> _subscription;

  @override
  void onInit() {
    super.onInit();

    final socketClientController = Get.find<SocketClientController>();
    _subscription = socketClientController.message.listen((data) {
      // print("message...$data");
    });
  }

  @override
  void onClose() {
    _subscription?.cancel();
    super.onClose();
  }

  void increment() {
    counter.value++;
    //update();
  }

  void getDate() {
    currentDate.value = DateTime.now().toString();
  }

  void addItem() {
    items.add(DateTime.now().toString());
  }

  void removeItem(int index) {
    items.removeAt(index);
  }

  void addMapItem() {
    final String entrada = DateTime.now().toString();
    mapItems.addIf(true, entrada, entrada);
  }

  void removeMapItem(String key) {
    mapItems.remove(key);
  }

  void setAge(int age) {
    // myPet.value = myPet.value.copyWith(age: age);
    myPet.age = age;
  }
}
