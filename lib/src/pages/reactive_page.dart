import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart'; //esta es la importacion adecuada
import 'package:taxiapp/src/controllers/reactive_controller.dart';
import 'package:taxiapp/src/controllers/socket_client_controller.dart';

class ReactivePage extends StatelessWidget {
  const ReactivePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final socketClientController = Get.find<SocketClientController>();

    return GetBuilder<ReactiveController>(
      init: ReactiveController(),
      initState: (_) {},
      builder: (_) {
        return Scaffold(
            body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CupertinoTextField(
                onChanged: (text) {
                  socketClientController.setSearchText(text);
                },
              ),
              Obx(() => Text('Age ${socketClientController.message.value}')),
              Obx(() => Text('Age ${_.myPet.age}')),
              FlatButton(
                  color: Colors.black,
                  onPressed: () {
                    _.setAge(_.myPet.age + 1);
                  },
                  child: Text(
                    'Set age',
                    style: TextStyle(color: Colors.white),
                  ))
            ],
          ),
        )
            /*Obx(() => ListView(
                      children: _.mapItems.values
                          .map((e) => ListTile(
                                title: Text(e),
                                trailing: IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () {
                                      _.removeMapItem(e);
                                    }),
                              ))
                          .toList(),
                    )*/

            /*ListView.builder(
                  itemBuilder: (__, index) {
                    final String text = _.items[index];

                    return ListTile(
                      title: Text(text),
                      trailing: IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () {
                            _.removeItem(index);
                          }),
                    );
                  },
                  itemCount: _.items.length,
                )*/

            //),
            /*floatingActionButton: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                  heroTag: 'add',
                  child: Icon(Icons.add),
                  onPressed: () {
                    // _.increment();
                    // _.addItem();
                    _.addMapItem();
                  },
                ),
                FloatingActionButton(
                  heroTag: 'date',
                  child: Icon(Icons.calendar_today),
                  onPressed: () {
                    _.getDate();
                  },
                ),
              ],
            )*/
            );
      },
    );
  }
}
