import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/route_manager.dart';
import 'package:taxiapp/src/controllers/profile_controller.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
        init: ProfileController(),
        builder: (_) => Scaffold(
              appBar: AppBar(
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      })),
              body: SafeArea(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('${_.user.firstName} ${_.user.lastName}'),
                      SizedBox(
                        height: 10,
                      ),
                      CupertinoTextField(
                        onChanged: _.onInputTextChange,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CupertinoButton(
                          child: Text('Aceptar'),
                          onPressed: () {
                            _.goToBckWithData();
                          })
                    ],
                  ),
                ),
              ),
            ));
  }
}
