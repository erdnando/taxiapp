import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:taxiapp/src/controllers/global_controller.dart';
import 'package:taxiapp/src/controllers/home_controller.dart';
import 'package:taxiapp/src/widgets/home_list.dart';
import 'package:taxiapp/src/widgets/product_list.dart';

class HomePage extends StatelessWidget {
  // const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_) => Scaffold(
        appBar: AppBar(
          actions: [
            GetBuilder<GlobalController>(
                id: 'favorites',
                builder: (_) => FlatButton(
                    child: Text('Favoritos (${_.favorites.length})'),
                    onPressed: () {}))
          ],
        ),
        // body: HomeList(),
        body: ProductList(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _.increment();
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
