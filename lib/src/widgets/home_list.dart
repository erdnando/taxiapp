import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:taxiapp/src/controllers/home_controller.dart';
import 'package:taxiapp/src/models/user.dart';

class HomeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        id: 'users',
        builder: (_) {
          if (_.loading == true)
            return Center(child: CircularProgressIndicator());
          else
            return ListView.builder(
              itemCount: _.users.length,
              itemBuilder: (context, index) {
                final User user = _.users[index];
                return ListTile(
                  title: Text(user.firstName),
                  subtitle: Text(user.email),
                  onTap: () => _.showUserProfile(user),
                );
              },
            );
        });
  }
}
