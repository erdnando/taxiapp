import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:taxiapp/src/controllers/global_controller.dart';
import 'package:taxiapp/src/models/product.dart';

class ProductList extends StatelessWidget {
  const ProductList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final controller =  Get.find<GlobalController>();
    return GetBuilder<GlobalController>(
        id: 'products',
        builder: (_) => ListView.builder(
              itemBuilder: (__, index) {
                final Product product = _.products[index];
                return GestureDetector(
                  onTap: () {
                    _.onFavorite(index, !product.isFavorite);
                  },
                  child: ListTile(
                    title: Text(product.name),
                    subtitle: Text('USD ${product.price}'),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.favorite,
                        color: product.isFavorite ? Colors.blue : Colors.grey,
                      ),
                      onPressed: () {
                        _.onFavorite(index, !product.isFavorite);
                      },
                    ),
                  ),
                );
              },
              itemCount: _.products.length,
            ));
  }
}
